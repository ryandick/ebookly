var logme = require('logme');
var util = require("util");
var log = {
	er: function(m) {
		logme.critical(m);
	},
	warn: function(m) {
		logme.warning(m);
	},
	info: function(m) {
		logme.info(m);
	},
	debug: function(m) {
		logme.info(m);
	},
	inspect: function(obj) {
		logme.debug(util.inspect(obj));
	}
};
exports.logger = log;