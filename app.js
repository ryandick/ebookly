var fs = require('fs'),
mongo = require('mongojs'),
	crypto = require('crypto'),
	rs, logme = require('logme'),
	util = require("util"),
	config, log = {
		er: function(m) {
			logme.critical(m);
		},
		warn: function(m) {
			logme.warning(m);
		},
		info: function(m) {
			logme.info(m);
		},
		debug: function(m) {
			logme.info(m);
		},
		inspect: function(obj) {
			logme.debug(util.inspect(obj));
		}
	},
	fileQ = [{}],
	ebooks = [{}],
	EPub = require("epub");
//walk the ebooks directory for supported file extension
ebooks.shift();
function scanForEbooks() {
	log.info('scanning...');
	fs.readdir(config.ebooks_path, function(err, files) {
		if (err) {
			log.er('er:' + err);
		} else if (files) {
			for (var i in files) {
					var file = {
						path: files[i],
						found: new Date().getTime(),
						hash: null,
						fid: genuid()
					};
					log.inspect(file);

					//check if file is one of the a handled extension
					if (endsWith('.epub', file.path)) {
						fileQ.push(file);
						log.info("#" + fileQ.length + " found:" + file.path);
					} else {
						log.warn("Skipping: " + file.path);
					}
			}
		} else {
			log.critical('shooting blanks all over ebook dir');
		}
		mongo.ebooks.save(fileQ);
	});
}
//get hash for file content

function hash(filename, hashtype) {
	var hasher = crypto.createHash(hashtype);
	var s = fs.ReadStream(filename);
	s.on('data', function(d) {
		hasher.update(d);
	});

	s.on('end', function() {
		var d = shasum.digest('hex');
		return d;
	});
}
//generate random uuid

function genuid(len) {
	if (!(len)) {
		//default
		len = 7;
	}
	return Math.random().toString(36).substring(len);
}
//test a path for specified extension

function endsWith(suffix, exten) {
	return String(suffix).indexOf(suffix, exten.length - suffix.length);
}

function readconfig(path, cb) {
	var defualts = {
		ebooks_path: "./ebooks/",
		port: 3535,
		imagewebroot: "./public/store/img/",
		chapterwebroot: "./public/store/chapters/"
	};
	if (!(path)) {
		path = "./config.json";
	}
	fs.readFile(path, function(err, content) {
		if (err) {
			log.er(err);
			//defaults
			log.warn('config falling back to default');
			cb(defaults);
		} else {
			if (content) {
				log.info('config loaded...');
				cb(JSON.parse(content.toString('utf8')));
			} else {
				//defaults
				log.warn('config falling back to default');
				cb(defaults);
			}
		}
	});
}

function getEbook(path, cb) {
	var epub = new EPub(path, config.imagewebroot, config.chapterwebroot);
}
readconfig('./config.json', function(configData) {
	config = configData;
	mongo = require('mongojs').connect(config.db.conStr, ['ebooks']);
});
